<?php the_content(); ?>

<?php $otzivi = get_field('otzivi');?>

<?php if( $otzivi ): ?>
<h3><?php the_title(); ?>, отзывы:</h3>

<ul class="no-bullet wpb_image_grid_ul row">
    <?php foreach( $otzivi as $image ): ?>
        <li class="margin-tiny">
        	 <a href="<?php echo $image['url']; ?>" class="prettyphoto" data-rel="prettyPhoto[<?php the_id(); ?>]"><?php echo wp_get_attachment_image( $image['ID'], 'thumbnail' ); ?></a>
        </li>
    <?php endforeach; ?>
</ul>

<?php endif; ?>


<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
