
<div class="contain-to-grid">
  
  <header>
    
    <div style="background: #f9f9f9;">
      <div class="row row-align-center">
        
        <div class="logo column small-6 medium-3">
          <a href="/"><img class="logo-img" src="<?php echo get_template_directory_uri();?>/dist/images/logo-white.png"></a>
        </div>
        
        <div class="grafik column shrink font-smaller">
          <?php the_field('grafik', 'option'); ?>
        </div>
        
        <div class="contact-header column padding-small-t lg-margin-medium-l">
          <a href="tel:<?php the_field('tel', 'option'); ?>" class="tel font-bold font-larger"><?php the_field('tel', 'option'); ?></a><br>
          <a href="https://api.whatsapp.com/send?phone=<?php the_field('whatsapp', 'option'); ?>" class="color-text">
            <i class="fa fa-whatsapp color-green margin-tiny-r"></i> <?php the_field('whatsapp', 'option'); ?>
          </a>
          <div class="address margin-no"><?php the_field('address', 'option'); ?></div>
        </div>
        
        <div class="column shrink text-right">
  <!--         <button class="button primary" data-open="call-back">Заказать звонок</button> -->
          <button class="button bg-coral margin-no" data-open="form-priem">Обратный звонок</button>
        </div>
      </div>
      
    </div>
    
    <div class="site-title-bar title-bar" data-responsive-toggle="mobile-menu">
      <span class="margin-tiny-rl">Меню услуг</span>
      <button class="menu-icon" type="button" data-toggle="mobile-menu"></button>
		</div>
      
    <nav class="site-navigation main-menu" id="mobile-menu" data-topbar role="navigation">
      
      <div class="row centered-content">
        
        <?php if (has_nav_menu('primary_navigation')) :
          wp_nav_menu([
          'theme_location' => 'primary_navigation', 
          'menu_class' => 'nav menu column small-12', 
          'container' => false,
          'walker' => new Submenu_Walker_Nav_Menu
          ]);
        endif; ?>
        
      </div>
    </nav>
  </header>
</div> <!-- contain-to-grid -->