
<div class="columns small-12 medium-4">
  <?php if ( has_post_thumbnail()) {the_post_thumbnail("full",array("class"=>"full-doctor-image"));} ?>
  
  <p class="doc-position"><?php the_field('positiion') ?></p>
  <div class="doc-about"><?php the_field('doc_about') ?></div>
  
</div>

<div class="columns small-12 medium-8 lg-padding-medium-rl">
  <h1 class="doc-header"><?php the_title(); ?></h1>
  <?php the_content(); ?>
</div>


