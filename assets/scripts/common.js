var Common = (function($) {

  var _functions = function(e) {
    
    var doc = /\.doc/gi;
    var pdf = /\.pdf\b/gi;
    var photo = /\.jpg|jpeg|png\b/gi;
    
    $('.main a').each(function(){
      var hrefType = $(this).attr('href');

      if(hrefType.match(doc)){

        $(this).append('<i class="fa fa-file-word-o js-added"></i><i class="fa fa-download js-added"></i>');
      }
      if(hrefType.match(pdf)){

        $(this).append('<i class="fa fa-file-pdf-o js-added"></i>');
      }
      if(hrefType.match(photo)){

        $(this).append('<i class="fa fa-file-photo-o js-added"></i>');
      }
    });

  };


  return {
    initialize: function() {
      _functions();
    }
  };
})(jQuery);
