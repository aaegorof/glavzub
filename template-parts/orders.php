<div class="reveal" id="form-priem" data-reveal>
  <div class="h3 margin-medium-b text-center">Перезвоните мне</div>

  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
  
  <?php echo do_shortcode('[contact-form-7 id="133" title="Перезвоните мне"]'); ?>
</div>


<div class="reveal" id="call-back" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">
  <div class="h3 margin-medium-b text-center">Свяжитесь со мной</div>
  <?php
  echo do_shortcode('[contact-form-7 id="8" title="Контактная форма 1"]'); ?>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>