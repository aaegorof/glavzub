<div class="reveal" id="form-priem" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">
  <div class="header-2 text-center">Перезвоните мне</div>
  <?php
  echo do_shortcode('[contact-form-7 id="133" title="Перезвоните мне"]'); ?>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

<div class="reveal" id="call-back" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">
  <div class="header-2 text-center">Перезвоните мне</div>
  <?php
  echo do_shortcode('[contact-form-7 id="132" title="Перезвоните мне"]'); ?>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>