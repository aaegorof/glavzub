<?php  
use Roots\Sage\Config;
use Roots\Sage\Wrapper;
?>

<?php while (have_posts()) : the_post(); ?>
<?php get_template_part('templates/service', 'header'); ?>
<?php endwhile; ?>

<div class="content content-service row">
  <main class="main small-12 columns <?php if (Config\display_sidebar()) echo 'medium-8'; ?>" role="main">

    <?php while (have_posts()) : the_post(); ?>

    <?php get_template_part('templates/content', 'service'); ?>

<?php $id = get_the_id();
  $terms = get_the_term_list( $id, 'specialization' );
  $terms = strip_tags( $terms );?>
    
    
    <?php endwhile; ?>
  </main><!-- /.main -->
  
  <?php if (Config\display_sidebar()) : ?>
    <aside class="sidebar small-12 medium-4 columns" role="complementary">
      <?php include Wrapper\sidebar_path(); ?>
    </aside><!-- /.sidebar -->
  <?php endif; ?>
</div><!-- /.content -->

