<?php  
use Roots\Sage\Config;
use Roots\Sage\Wrapper;
?>

<div class="content content-doctor row">
  <main class="main row small-12 columns <?php if (Config\display_sidebar()) echo 'medium-8'; ?>" role="main">

    <?php while (have_posts()) : the_post(); ?>

    <?php get_template_part('templates/content', 'doctor'); ?>
    
    
    <?php endwhile; ?>
  </main><!-- /.main -->
  
  <?php if (Config\display_sidebar()) : ?>
    <aside class="sidebar small-12 medium-4 columns" role="complementary">
      <?php include Wrapper\sidebar_path(); ?>
    </aside><!-- /.sidebar -->
  <?php endif; ?>
</div><!-- /.content -->

